//
//  UIFont+CustomFonts.m
//  RecipeCustomObj
//
//  Created by Adam Farrell on 6/8/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "UIFont+CustomFonts.h"

@implementation UIFont (CustomFonts)

+(UIFont*)primaryTitleFont {
    return [UIFont systemFontOfSize:24 weight:bold];
}

+(UIFont*)primaryFont {
    return [UIFont systemFontOfSize:16];
}

@end
