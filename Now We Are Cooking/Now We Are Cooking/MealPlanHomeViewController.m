//
//  MealPlanHomeViewController.m
//  Now We Are Cooking
//
//  Created by Adam Farrell on 8/10/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "MealPlanHomeViewController.h"

@interface MealPlanHomeViewController ()

@end

@implementation MealPlanHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tabBarController.tabBar setBarStyle:UIBarStyleBlack];
    self.tabBarController.tabBar.barTintColor = [UIColor tabBarTint];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
