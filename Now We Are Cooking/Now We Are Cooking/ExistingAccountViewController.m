//
//  ExistingAccountViewController.m
//  Now We Are Cooking
//
//  Created by Adam Farrell on 7/22/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "ExistingAccountViewController.h"

@interface ExistingAccountViewController ()

@end

@implementation ExistingAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor primaryBackgroundColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont primaryTitleFont], NSFontAttributeName, nil]];
    
    //background image
    UIImageView* background = [[UIImageView alloc]initWithFrame:self.view.bounds];
    background.image = [UIImage imageNamed:@"foodiesfeed.com_Gunel-Farhadli-2.jpg"];
    [self.view addSubview:background];
    
    userName = [UITextField new];
    userName.translatesAutoresizingMaskIntoConstraints = NO;
    userName.backgroundColor = [UIColor primaryTextFieldBackgroundColor];
    userName.textColor = [UIColor primaryTextFieldFontColor];
    userName.font = [UIFont primaryTextFieldFont];
    userName.placeholder = @"User Name";
    [userName setLeftViewMode:UITextFieldViewModeAlways];
    UIView* spacer1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 10)];
    [userName setLeftView:spacer1];
    userName.keyboardAppearance = UIKeyboardAppearanceDark;
    userName.delegate = self;
    [self.view addSubview:userName];
    
    password = [UITextField new];
    password.translatesAutoresizingMaskIntoConstraints = NO;
    password.secureTextEntry = YES;
    password.backgroundColor = [UIColor primaryTextFieldBackgroundColor];
    password.textColor = [UIColor primaryTextFieldFontColor];
    password.font = [UIFont primaryTextFieldFont];
    password.placeholder = @"Password";
    [password setLeftViewMode:UITextFieldViewModeAlways];
    UIView* spacer2 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 10)];
    [password setLeftView:spacer2];
    password.keyboardAppearance = UIKeyboardAppearanceDark;
    password.delegate = self;
    [self.view addSubview:password];
    
    CustomButton* logIn = [CustomButton new];
    logIn.translatesAutoresizingMaskIntoConstraints = NO;
    [logIn setTitle:@"Log In" forState:UIControlStateNormal];
    [logIn addTarget:self action:@selector(loginTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logIn];
    
    NSDictionary* views = NSDictionaryOfVariableBindings(userName, password, logIn);
    NSDictionary* metrics = @{@"padding":@10.0,@"fromTop":@80.0,@"smallHeight":@45.0,@"imageHeight":@150.0,@"paddingx3":@30.0};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-paddingx3-[userName]-paddingx3-|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-paddingx3-[password]-paddingx3-|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-paddingx3-[logIn]-paddingx3-|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-<=fromTop-[userName(smallHeight)]-paddingx3-[password(smallHeight)]-paddingx3-[logIn(smallHeight)]->=fromTop-|" options:NSLayoutFormatAlignAllCenterX metrics:metrics views:views]];
}

-(void)loginTouched:(id)sender {
    [PFUser logInWithUsernameInBackground:[userName.text lowercaseString] password:password.text block:^(PFUser* user, NSError* error){
        if (user) {
            HomeViewController* hvc = [HomeViewController new];
            [self.navigationController pushViewController:hvc animated:YES];
        } else {
            NSString* errorString = [error userInfo][@"error"];
            UIAlertController* errorAlert = [UIAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){}];
            [errorAlert addAction:okAction];
            [self presentViewController:errorAlert animated:YES completion:nil];
        }
    }];
    
//    HomeViewController* hvc = [HomeViewController new];
//    self.title = @"Log Out";
//    [self.navigationController pushViewController:hvc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    self.title = @"Log In";
    [super viewWillAppear:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
