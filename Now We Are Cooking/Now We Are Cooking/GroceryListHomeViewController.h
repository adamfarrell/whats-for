//
//  GroceryListHomeViewController.h
//  Now We Are Cooking
//
//  Created by Adam Farrell on 8/10/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "UIColor+CustomColors.h"
#import "UIFont+CustomFonts.h"
#import "CustomButton.h"
#import "HomeViewController.h"
#import "EditNewGrocListViewController.h"

@interface GroceryListHomeViewController : UITableViewController<UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate>
{
    NSIndexPath* edit;
    BOOL hasNavigatedFurther;
}
//test code, remove later!!!
@property (nonatomic, strong) NSMutableArray* testArr;
//end test code
@end
