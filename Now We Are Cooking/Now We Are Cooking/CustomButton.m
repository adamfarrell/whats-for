//
//  CustomButton.m
//  RecipeCustomObj
//
//  Created by Adam Farrell on 6/8/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "CustomButton.h"

@implementation CustomButton

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor primaryButtonColor];
//        self.titleLabel.textColor = [UIColor blueColor];
//        self.titleLabel.tintColor = [UIColor blueColor];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont primaryButtonFont];
        [self setTitleColor:[UIColor primaryButtonFontColor] forState:UIControlStateNormal];
//        [self setTintColor:[UIColor blueColor]];
    }
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
