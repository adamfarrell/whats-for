//
//  CreateViewController.m
//  Now We Are Cooking
//
//  Created by Adam Farrell on 7/22/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "CreateAcctViewController.h"

@interface CreateAcctViewController ()

@end

@implementation CreateAcctViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor primaryBackgroundColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont primaryTitleFont], NSFontAttributeName, nil]];
    self.title = @"Create Account";
    
    //background image
    UIImageView* background = [[UIImageView alloc]initWithFrame:self.view.bounds];
    background.image = [UIImage imageNamed:@"foodiesfeed.com_Gunel-Farhadli-2.jpg"];
    [self.view addSubview:background];
    
//    UIScrollView* scroll = [UIScrollView new];
//    scroll.translatesAutoresizingMaskIntoConstraints = NO;
//    scroll.backgroundColor = [UIColor primaryBackgroundColor];
//    scroll.delegate = self;
//    [self.view addSubview:scroll];
    
    //user name
    userName = [UITextField new];
    userName.translatesAutoresizingMaskIntoConstraints = NO;
    userName.backgroundColor = [UIColor primaryTextFieldBackgroundColor];
    userName.textColor = [UIColor primaryTextFieldFontColor];
    userName.font = [UIFont primaryTextFieldFont];
    userName.placeholder = @"User Name";
    [userName setLeftViewMode:UITextFieldViewModeAlways];
    UIView* spacer1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 10)];
    [userName setLeftView:spacer1];
    userName.keyboardAppearance = UIKeyboardAppearanceDark;
    userName.delegate = self;
    [self.view addSubview:userName];
    
    //password
    password = [UITextField new];
    password.secureTextEntry = YES;
    password.translatesAutoresizingMaskIntoConstraints = NO;
    password.backgroundColor = [UIColor primaryTextFieldBackgroundColor];
    password.textColor = [UIColor primaryTextFieldFontColor];
    password.font = [UIFont primaryTextFieldFont];
    password.placeholder = @"Password";
    [password setLeftViewMode:UITextFieldViewModeAlways];
    UIView* spacer2 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 10)];
    [password setLeftView:spacer2];
    password.keyboardAppearance = UIKeyboardAppearanceDark;
    password.delegate = self;
    [self.view addSubview:password];
    
    //password confirmation
    passwordConf = [UITextField new];
    passwordConf.secureTextEntry = YES;
    passwordConf.translatesAutoresizingMaskIntoConstraints = NO;
    passwordConf.backgroundColor = [UIColor primaryTextFieldBackgroundColor];
    passwordConf.textColor = [UIColor primaryTextFieldFontColor];
    passwordConf.font = [UIFont primaryTextFieldFont];
    passwordConf.placeholder = @"Confirm Password";
    [passwordConf setLeftViewMode:UITextFieldViewModeAlways];
    UIView* spacer3 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 10)];
    [passwordConf setLeftView:spacer3];
    passwordConf.keyboardAppearance = UIKeyboardAppearanceDark;
    passwordConf.delegate = self;
    [self.view addSubview:passwordConf];
    
    //email
    email = [UITextField new];
    email.translatesAutoresizingMaskIntoConstraints = NO;
    email.backgroundColor = [UIColor primaryTextFieldBackgroundColor];
    email.textColor = [UIColor primaryTextFieldFontColor];
    email.font = [UIFont primaryTextFieldFont];
    email.placeholder = @"Email Address";
    [email setLeftViewMode:UITextFieldViewModeAlways];
    UIView* spacer4 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 10)];
    [email setLeftView:spacer4];
    email.keyboardAppearance = UIKeyboardAppearanceDark;
    email.keyboardType = UIKeyboardTypeEmailAddress;
    email.delegate = self;
    [self.view addSubview:email];
    
    //confirm button
    CustomButton* create = [CustomButton new];
    create.translatesAutoresizingMaskIntoConstraints = NO;
    [create setTitle:@"Create Account" forState:UIControlStateNormal];
    [create addTarget:self action:@selector(createTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:create];
    
    NSDictionary* views = NSDictionaryOfVariableBindings(userName, password, create, passwordConf, email);
    NSDictionary* metrics = @{@"padding":@10.0,@"fromTop":@80.0,@"smallHeight":@45.0,@"imageHeight":@150.0,@"paddingx3":@30.0};
    
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[scroll]-0-|" options:0 metrics:metrics views:views]];
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[scroll]-0-|" options:NSLayoutFormatAlignAllCenterX metrics:metrics views:views]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-paddingx3-[userName(>=300.0)]-paddingx3-|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-paddingx3-[password]-paddingx3-|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-paddingx3-[passwordConf]-paddingx3-|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-paddingx3-[email]-paddingx3-|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-paddingx3-[create]-paddingx3-|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-<=fromTop-[userName(smallHeight)]-paddingx3-[password(smallHeight)]-paddingx3-[passwordConf(smallHeight)]-paddingx3-[email(smallHeight)]-paddingx3-[create(smallHeight)]->=fromTop-|" options:NSLayoutFormatAlignAllCenterX metrics:metrics views:views]];
}

-(void)createTouched:(id)sender {
    if ([password.text isEqualToString:passwordConf.text]) {
        PFUser* user = [PFUser user];
        user.username = [userName.text lowercaseString];
        user.password = password.text;
        user.email = [email.text lowercaseString];
        
        [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError* error) {
            if (!error) {
                HomeViewController* hvc = [HomeViewController new];
                [self.navigationController pushViewController:hvc animated:YES];
            } else {
                NSString* errorString = [error userInfo][@"error"];
                UIAlertController* errorAlert = [UIAlertController alertControllerWithTitle:@"Error" message:errorString preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){}];
                [errorAlert addAction:okAction];
                [self presentViewController:errorAlert animated:YES completion:nil];
            }
        }];
        
        
//        ExistingAccountViewController* eavc = [ExistingAccountViewController new];
//        [self.navigationController popViewControllerAnimated:YES];
//        [self.navigationController pushViewController:eavc animated:YES];
    } else {
        UIAlertController* noMatch = [UIAlertController alertControllerWithTitle:@"Passwords do not match" message:@"Please try again" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action)
        {
//            password.text = @"";
//            passwordConf.text = @"";
        }];
        [noMatch addAction:okAction];
        [self presentViewController:noMatch animated:YES completion:nil];
        password.text = @"";
        passwordConf.text = @"";
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
