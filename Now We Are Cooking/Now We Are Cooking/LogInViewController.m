//
//  ViewController.m
//  Now We Are Cooking
//
//  Created by Adam Farrell on 7/21/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "LogInViewController.h"

@interface LogInViewController ()

@end

@implementation LogInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
//    //begin parse test
//    PFObject *testObject = [PFObject objectWithClassName:@"TestObject"];
//    testObject[@"foo"] = @"bar";
//    [testObject saveInBackground];
//    //end parse test
    
    if ([self checkForCurrentUser]) {
        HomeViewController* hvc = [HomeViewController new];
        [self.navigationController pushViewController:hvc animated:NO];
    }
    
    //set up bars
    self.view.backgroundColor = [UIColor primaryBackgroundColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont primaryTitleFont], NSFontAttributeName, nil]];
//    self.title = @"Sign In";
    
    //background image
    UIImageView* background = [[UIImageView alloc]initWithFrame:self.view.bounds];
    background.image = [UIImage imageNamed:@"foodiesfeed.com_Gunel-Farhadli-2.jpg"];
    [self.view addSubview:background];
    
    //will be positioned near the top
    UILabel* lbl = [UILabel new];
    lbl.translatesAutoresizingMaskIntoConstraints = NO;
    lbl.font = [UIFont primarySubTitleFont];
    lbl.textColor = [UIColor primaryFontColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.text = @"Now We're Cooking!";
    [self.view addSubview:lbl];
    
//    //will be position below the title, should take up most of the space of screen.
//    UIImageView* img = [UIImageView new];
//    img.translatesAutoresizingMaskIntoConstraints = NO;
//    img.contentMode = UIViewContentModeScaleAspectFit;
//    img.image = [UIImage imageNamed:@"plate-of-food"];
//    [self.view addSubview:img];
    
    
    //will be positioned at bottom just above log in button
    CustomButton* createAcct = [CustomButton new];
    createAcct.translatesAutoresizingMaskIntoConstraints = NO;
    [createAcct setTitle:@"Create Account" forState:UIControlStateNormal];
    createAcct.backgroundColor = [UIColor secondaryButtonColor];
//    createAcct.titleLabel.textColor = [UIColor secondaryButtonFontColor];
    [createAcct setTitleColor:[UIColor secondaryButtonFontColor] forState:UIControlStateNormal];
    [createAcct addTarget:self action:@selector(createAcctTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:createAcct];
    
    //will be positioned at very bottom
    CustomButton* logIn = [CustomButton new];
    logIn.translatesAutoresizingMaskIntoConstraints = NO;
    [logIn setTitle:@"Log In" forState:UIControlStateNormal];
    [logIn addTarget:self action:@selector(logInTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logIn];
    
    //set up for autolayout
    float width = self.view.bounds.size.width;
    NSNumber* portWidth = [NSNumber numberWithFloat:width];
    NSDictionary* views = NSDictionaryOfVariableBindings(lbl, /*img,*/ createAcct, logIn);
    NSDictionary* metrics = @{@"padding":@10.0,@"fromTop":@80.0,@"smallHeight":@45.0,@"imageHeight":@150,@"paddingx3":@30.0,@"portWidth":portWidth};
    
    //autolayout components
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-padding-[lbl]-padding-|" options:0 metrics:metrics views:views]];
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-padding-[img]-padding-|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|->=0-[createAcct(>=portWidth)]->=0-|" options:0 metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|->=0-[logIn(>=portWidth)]->=0-|" options:0 metrics:metrics views:views]];
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-<=fromTop-[lbl(smallHeight)]-padding-[img(<=imageHeight)]->=paddingx3@200-[createAcct(smallHeight)][logIn(smallHeight)]|" options:NSLayoutFormatAlignAllCenterX metrics:metrics views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-<=fromTop-[lbl(smallHeight)]->=paddingx3@200-[createAcct(smallHeight)][logIn(smallHeight)]|" options:NSLayoutFormatAlignAllCenterX metrics:metrics views:views]];
    
    
//    UILabel* mySubTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 70, self.view.bounds.size.width, 50)];
//    mySubTitle.font = [UIFont primarySubTitleFont];
//    mySubTitle.text = @"This is a Subtitle";
//    mySubTitle.textColor = [UIColor whiteColor];
//    mySubTitle.textAlignment = NSTextAlignmentCenter;
//    [self.view addSubview:mySubTitle];
//    
//    UITextView* paragraph = [[UITextView alloc]initWithFrame:CGRectMake(20, 140, self.view.bounds.size.width - 40, 200)];
//    paragraph.font = [UIFont primaryFont];
//    paragraph.text = @"This is some dummy text. Ooh boy, it is some dumb dummy text. This is some dummy text. Ooh boy, it is some dumb dummy text. This is some dummy text. Ooh boy, it is some dumb dummy text. This is some dummy text. Ooh boy, it is some dumb dummy text. This is some dummy text. Ooh boy, it is some dumb dummy text.";
//    paragraph.textColor = [UIColor whiteColor];
//    paragraph.backgroundColor = [UIColor primaryBackgroundColor];
//    [self.view addSubview:paragraph];
    
//    self.tabBar.barStyle = UIBarStyleBlack;
    
//    for (NSString* family in [UIFont familyNames])
//    {
//        NSLog(@"%@", family);
//        
//        for (NSString* name in [UIFont fontNamesForFamilyName: family])
//        {
//            NSLog(@"  %@", name);
//        }
//    }
    
    
}

-(BOOL)checkForCurrentUser {
    PFUser* currentUser = [PFUser currentUser];
    if (currentUser) {
        return YES;
    } else {
        return NO;
    }
}

-(void)logInTouched:(id)sender {
    ExistingAccountViewController* eavc = [ExistingAccountViewController new];
    [self.navigationController pushViewController:eavc animated:YES];
}

-(void)createAcctTouched:(id)sender {
    CreateAcctViewController* cavc = [CreateAcctViewController new];
    [self.navigationController pushViewController:cavc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
