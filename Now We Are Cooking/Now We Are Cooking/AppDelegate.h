//
//  AppDelegate.h
//  Now We Are Cooking
//
//  Created by Adam Farrell on 7/21/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

