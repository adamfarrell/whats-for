//
//  GroceryListHomeViewController.m
//  Now We Are Cooking
//
//  Created by Adam Farrell on 8/10/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "GroceryListHomeViewController.h"

@interface GroceryListHomeViewController ()

@end

@implementation GroceryListHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tabBarController.tabBar setBarStyle:UIBarStyleBlack];
    self.tabBarController.tabBar.barTintColor = [UIColor tabBarTint];
    hasNavigatedFurther = NO;
    
    //set up tableview
    UITableView* tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.backgroundColor = [UIColor primaryBackgroundColor];
    [self.view addSubview:tableView];
    self.view.backgroundColor = [UIColor primaryBackgroundColor];
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
//    UISwipeGestureRecognizer* gestureRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipeRight:)];
//    [gestureRight setDelegate:self];
//    [self.tableView addGestureRecognizer:gestureRight];
    //test code, remove later!!!
    NSArray* temp = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10"];
    _testArr = [[NSMutableArray alloc]initWithArray:temp];
    //end test code
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addTouched:)];
    if (!hasNavigatedFurther) {
        self.tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
    } else {
        self.tableView.contentInset = UIEdgeInsetsZero;
        hasNavigatedFurther = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)handleSwipeRight:(UISwipeGestureRecognizer*)gestureRecognizer {
    CGPoint point = [gestureRecognizer locationInView:self.tableView];
    edit = [self.tableView indexPathForRowAtPoint:point];
    NSLog(@"Swiped");
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //add code to edit an existing list, this is not nearly ready for implementation
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_testArr removeObjectAtIndex:indexPath.row];
        [tableView beginUpdates];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [tableView endUpdates];
        [self.tableView reloadData];
        NSLog(@"Should be delete");
        NSLog(@"arr: %@",_testArr);
//        [self.tableView reloadData];
    }
}
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == edit.section) {
//        if (indexPath.row == edit.row) {
//            NSLog(@"Edit time");
//            return YES;
//        }
//    }
//    return NO;
    return YES;
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.backgroundColor = [UIColor primaryBackgroundColor];
    //test code, remove later!!!
    cell.textLabel.text = _testArr[indexPath.row];
    //end test code
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //test code, remove later!!!
    return [_testArr count];
    //end test code
}

-(void)addTouched:(id)sender {
    hasNavigatedFurther = YES;
    EditNewGrocListViewController* shopList = [EditNewGrocListViewController new];
    [self.navigationController pushViewController:shopList animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
