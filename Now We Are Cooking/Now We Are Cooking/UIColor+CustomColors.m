//
//  UIColor+CustomColors.m
//  RecipeCustomObj
//
//  Created by Adam Farrell on 6/8/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "UIColor+CustomColors.h"

@implementation UIColor (CustomColors)

+(UIColor*)primaryBackgroundColor {
//    return [UIColor colorWithRed:43.0 / 255.0 green:43.0 / 255.0 blue:43.0 / 255.0 alpha:1.0];
    return [UIColor colorWithRed:232.0 / 255.0 green:221.0 / 255.0 blue:150.0 / 255.0 alpha:1.0];
}

+(UIColor*)primaryButtonFontColor {
    return [UIColor colorWithRed:250.0 / 255.0 green:250.0 / 255.0 blue:250.0 / 255.0 alpha:1.0];
}

+(UIColor*)primaryButtonColor {
    return [UIColor colorWithRed:255.0 / 255.0 green:151.0 / 255.0 blue:0.0 / 255.0 alpha:1.0];
}

+(UIColor*)secondaryButtonColor {
    return [UIColor colorWithRed:194.0 / 255.0 green:28.0 / 255.0 blue:24.0 / 255.0 alpha:1.0];
}

+(UIColor*)primaryFontColor {
    return [UIColor colorWithRed:250.0 / 255.0 green:250.0 / 255.0 blue:250.0 / 255.0 alpha:1.0];
}

+(UIColor*)secondaryButtonFontColor {
    return [UIColor colorWithRed:250.0 / 255.0 green:250.0 / 255.0 blue:250.0 / 255.0 alpha:1.0];
}

+(UIColor*)primaryTextFieldBackgroundColor {
    return [UIColor colorWithRed:250.0 / 255.0 green:250.0 / 255.0 blue:250.0 / 255.0 alpha:1.0];
}

+(UIColor*)primaryTextFieldFontColor {
    return [UIColor colorWithRed:43.0 / 255.0 green:43.0 / 255.0 blue:43.0 / 255.0 alpha:1.0];
}

+(UIColor*)activeTabFontColor {
    return [UIColor colorWithRed:1.0 green:151.0 / 255.0 blue:0.0 alpha:1.0];
}

+(UIColor*)defaultTabFontColor {
//    return [UIColor colorWithRed:232.0 / 255.0 green:221.0 / 255.0 blue:150.0 / 255.0 alpha:1.0];
    return [UIColor whiteColor];
}

+(UIColor*)tabBarTint {
//    return [UIColor colorWithRed:194.0 / 255.0 green:28.0 / 255.0 blue:24.0 / 255.0 alpha:.1];
//    return [UIColor colorWithRed:13.0 / 255.0 green:5.0 / 255.0 blue:18.0 / 255.0 alpha:.3];
//    return [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:.01];
    return [UIColor colorWithRed:0.337f green:0.325f blue:0.176f alpha:1.00f];
}

+(UIColor*)primaryPressButtonColor {
    return [UIColor colorWithRed:194.0f / 255.0f green:115.0f / 255.0f blue:0.0f alpha:1.0f];
}

+(UIColor*)primaryPressButtonFontColor {
    return [UIColor colorWithRed:214.0f / 255.0f green:210.0f / 255.0f blue:210.0f / 255.0f alpha:1.0];
}

@end
