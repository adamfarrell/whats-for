//
//  UIFont+CustomFonts.m
//  RecipeCustomObj
//
//  Created by Adam Farrell on 6/8/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "UIFont+CustomFonts.h"

@implementation UIFont (CustomFonts)

+(UIFont*)primaryTitleFont {
//    return [UIFont fontWithName:@"SeaweedScript-Regular" size:24];
    return [UIFont fontWithName:@"Pacifico-Regular" size:24];
}
+(UIFont*)primarySubTitleFont {
//    return [UIFont fontWithName:@"AmaticSC-Regular" size:18];
//    return [UIFont fontWithName:@"AmaticSC-Bold" size:32];
//    return [UIFont fontWithName:@"TheGirlNextDoor" size:40];
    return [UIFont fontWithName:@"Pacifico-Regular" size:40];
}
+(UIFont*)primaryFont {
    return [UIFont fontWithName:@"Nunito-Regular" size:14];
}

+(UIFont*)primaryButtonFont {
    return [UIFont fontWithName:@"Nunito-Bold" size:24];
}

+(UIFont*)primaryTextFieldFont {
    return [UIFont fontWithName:@"Nunito-Bold" size:18];
}

+(UIFont*)primaryTabFont {
    return [UIFont fontWithName:@"Pacifico-Regular" size:18];
}

+(UIFont*)primaryNavBarButtonFont {
    return [UIFont fontWithName:@"Nunito-Regular" size:16];
}

@end
