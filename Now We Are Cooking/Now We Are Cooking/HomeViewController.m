//
//  HomeViewController.m
//  Now We Are Cooking
//
//  Created by Adam Farrell on 7/22/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor primaryBackgroundColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
//    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont primaryTitleFont], NSFontAttributeName, nil]];
    [self.navigationController.navigationItem.backBarButtonItem setTitle:@"Back"];
    self.title = @"Home";
    
    RecipeHomeViewController* recipeHome = [RecipeHomeViewController new];
    MealPlanHomeViewController* mealHome = [MealPlanHomeViewController new];
    GroceryListHomeViewController* grocHome = [GroceryListHomeViewController new];
    
    NSArray* tabViewControllers = @[recipeHome, mealHome, grocHome];
    [self setViewControllers:tabViewControllers];
    
    recipeHome.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"Recipes" image:nil tag:0];
    mealHome.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"Meal Plans" image:nil tag:1];
    grocHome.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"Grocery Lists" image:nil tag:2];
    
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    UIButton* back = [UIButton buttonWithType:UIButtonTypeCustom];
    [back setImage:nil forState:UIControlStateNormal];
    [back addTarget:self action:@selector(logOutTouched:) forControlEvents:UIControlEventTouchUpInside];
    [back setFrame:CGRectMake(0, 0, 94, 30)];
    back.titleLabel.font = [UIFont primaryNavBarButtonFont];
    back.titleLabel.textColor = [UIColor primaryButtonFontColor];
    [back setTitle:@"Log Out" forState:UIControlStateNormal];
    back.backgroundColor = [UIColor primaryButtonColor];
    back.layer.cornerRadius = 8;
    back.layer.borderWidth = 1;
    back.layer.borderColor = [UIColor primaryButtonFontColor].CGColor;

    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:back];
}

-(void)logOutTouched:(id)sender {
    [PFUser logOut];
//    PFUser *currentUser = [PFUser currentUser]; // this will now be nil
//    NSLog(@"%@",currentUser);
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
