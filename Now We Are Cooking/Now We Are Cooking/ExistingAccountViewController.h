//
//  ExistingAccountViewController.h
//  Now We Are Cooking
//
//  Created by Adam Farrell on 7/22/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "UIColor+CustomColors.h"
#import "UIFont+CustomFonts.h"
#import "CustomButton.h"
#import "LogInViewController.h"
#import "HomeViewController.h"
#import "ExistingAccountViewController.h"

@interface ExistingAccountViewController : UIViewController<UITextFieldDelegate>
{
    UITextField* userName;
    UITextField* password;
}
@end
