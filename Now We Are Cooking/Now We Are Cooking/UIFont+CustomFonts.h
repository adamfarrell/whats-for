//
//  UIFont+CustomFonts.h
//  RecipeCustomObj
//
//  Created by Adam Farrell on 6/8/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (CustomFonts)

+(UIFont*)primaryTitleFont;
+(UIFont*)primarySubTitleFont;
+(UIFont*)primaryFont;
+(UIFont*)primaryButtonFont;
+(UIFont*)primaryTextFieldFont;
+(UIFont*)primaryTabFont;
+(UIFont*)primaryNavBarButtonFont;
@end
